console.log('application entry point');

/*
  =======================================
  MODULES AND CONSTANTS
  =======================================
*/

var http = require('http'),
    fs = require('fs'),
    mime = require('mime'),
    db = require('./fake-db'),
    HOMEPAGE_HTML = '/views/index.html',
    NOT_FOUND_MSG = 'file not found';



/*
  =======================================
  SERVER STARTUP
  =======================================
*/


var server = http.createServer( handleRequest )
    .on( 'error', function(e) {
      console.log('SERVER ERROR: ' + e.message);
    })
    .listen( 3000 );
    console.log( 'Server running at http://localhost:3000/' );


/*
  =======================================
  HANDLERS
  =======================================
*/

function resCodeMsgEND( res, code, msg ) {
  res.statusCode = code || 404;   // DO NOT! throw err;
  res.statusMessage = msg || NOT_FOUND_MSG;
  console.log( '[ ERROR !!! ]', code, msg || NOT_FOUND_MSG );
  res.end();
}

function sendStaticFile( url_, res ) {

  var filePath,
      filePathFull, 
      fileMimeType;

  // if homepage - show special html file ('the View')
  filePath = (url_ === "/") ? HOMEPAGE_HTML : url_ ;
  filePathFull = __dirname + filePath ;   // == \1\home work\Artem Drobotenko [..+..] /public/js/libs/underscore.js

  fs.readFile( filePathFull, function (err, content) {
    if ( err ) return resCodeMsgEND( res, 404, filePath + " || TODO: ERROR_MOCK_STRING" );

    console.log( "found file:", filePath );
    fileMimeType = mime.lookup( filePathFull );   // application/octet-stream   application/javascript  application/vnd.groove-tool-template
    
    res.writeHead( 200, { "Content-Type": fileMimeType } );
    res.end( content );   // == write + end
  });


}


function dbEndResponseWithData( res, data ) {
  console.log( '[dbEndResponseWithData] data:', data );
  if (typeof data !== 'string') {
    data = JSON.stringify(data);
  }
  res.end( data );   // == write + end
}



function handleRequest (req, res) {

  /********
  REQUEST FLAGS:
    isStaticFile:   file URL --> to PUBLIC folder, or domain root
    isRESTAPI:      API URL --> to /api/users
  */

  var url_ = req.url;
  var isStaticFile = ( /^\/public/.test(url_) || url_ === '/' );
  var isRESTAPI = /^\/api\/users/.test(url_);

  if ( isStaticFile ) {
    sendStaticFile( url_, res );

  } else if ( isRESTAPI ) {
    console.log( 'requested REST API URL:', req.method, url_ );

    switch (url_) {

      case "/api/users":      
          switch (req.method) {

            case "GET":
              db.getCollection( function( err, collection ){
                if (err) return resCodeMsgEND( res, 500, 'server error' );
                
                // collection == [ '{...}' , '{...}' ]
                dbEndResponseWithData( res, collection );
              });
              break;

            case "POST":
              // http://stackoverflow.com/questions/15427220/how-to-handle-post-request-in-node-js
              var requestBody = '';

              req.on('data', function(data) {
                requestBody += data;
                if(requestBody.length > 1e7) {
                  res.writeHead(413, 'Request Entity Too Large', {'Content-Type': 'text/html'});
                  res.end('<!doctype html><html><head><title>413</title></head><body>413: Request Entity Too Large</body></html>');
                }

              });
              req.on('end', function() {
                // requestBody == {"firstName":"2","lastName":"2","company":"2","position":"2","email":"2","phoneNumber":"2"}

                db.create( requestBody, function( err, model ){
                  if (err) return resCodeMsgEND( res, 500, 'server error' );
                  dbEndResponseWithData( res, model );
                });

              });


              break;

            case "PUT":
              ""
              break;

            case "DELETE":
              ""
              break;

            default:
              ""
              break;
          }
        break;

      default: 
        resCodeMsgEND(res, 404, "[ERROR: REST API is not yet here]");
        return res.end();
        break;

    }


  } else {
    // url not found
    return resCodeMsgEND(res, 404, 'url not found');
  }

}

